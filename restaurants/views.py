from django.shortcuts import render,redirect
from .models import Restaurant
import os 

# Create your views here.
def index(request):
    gmap_api_key = os.environ.get("GMAP_API_KEY")
    restaurants = Restaurant.objects.all()
    return render(request,
        'restaurants/index.html',
        {'restaurants': restaurants,
          'gmap_api_key': gmap_api_key})

def detail(request, restaurant_id):
    gmap_api_key = os.environ.get("GMAP_API_KEY")
    restaurant = Restaurant.objects.filter(id=restaurant_id).first()
    return render(request,
        'restaurants/detail.html',
        {'restaurant': restaurant,
          'gmap_api_key': gmap_api_key}
    )

def create(request):
    if request.method == 'POST':
        restaurant = Restaurant(
            name=request.POST.get('name'),
            address=request.POST.get('address'),
            business_hour=request.POST.get('business_hour'),
            detail=request.POST.get('detail')
        )
        restaurant.save()
        return redirect('/restaurants/'+str(restaurant.id))

    return render(request,'restaurants/create.html')

def update(request, restaurant_id):
    restaurant = Restaurant.objects.filter(id=restaurant_id).first()
    if request.method == 'POST':
        restaurant.name=request.POST.get('name')
        restaurant.address=request.POST.get('address')
        restaurant.business_hour=request.POST.get('business_hour')
        restaurant.detail=request.POST.get('detail')
        restaurant.save()
        return redirect('/restaurants/'+str(restaurant.id))
    return render(request,
    'restaurants/update.html',
    {'restaurant': restaurant})

def delete(request, restaurant_id):
    restaurant = Restaurant.objects.filter(id=restaurant_id).first()
    restaurant.delete()
    return redirect('/')

